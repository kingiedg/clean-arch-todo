import '../entities/todo.dart';

abstract class TodoRepository {
  addTodo(Todo todo);
  getTodos();
}
