import 'dart:async';

import '../entities/todo.dart';
import '../repositories/todo_repository.dart';
import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';

class AddTodoUseCase extends UseCase<AddTodoUseCaseResponse, AddTodoUseCaseParams> {
  
  final TodoRepository todoRepository;
  AddTodoUseCase(this.todoRepository);


  @override
  Future<Stream<AddTodoUseCaseResponse>> buildUseCaseStream(
      AddTodoUseCaseParams params) async {
    final controller = StreamController<AddTodoUseCaseResponse>();
    try {
      // get user
      final todo = await todoRepository.addTodo(params.todo);
      // Adding it triggers the .onNext() in the `Observer`
      // It is usually better to wrap the reponse inside a respose object.
      controller.add(AddTodoUseCaseResponse(todo));
      logger.finest('AddTodoUseCase successful.');
      controller.close();
    } catch (e) {
      logger.severe('AddTodoUseCase unsuccessful.');
      // Trigger .onError
      controller.addError(e);
    }
    return controller.stream;
  }
}

class AddTodoUseCaseParams {
  final Todo todo;
  AddTodoUseCaseParams(this.todo);
}

class AddTodoUseCaseResponse {
  final Todo todo;
  AddTodoUseCaseResponse(this.todo);
}


class GetTodosUseCase extends UseCase<GetTodosUseCaseResponse, GetTodosUseCaseParams> {
  
  final TodoRepository todoRepository;
  GetTodosUseCase(this.todoRepository);


  @override
  Future<Stream<GetTodosUseCaseResponse>> buildUseCaseStream(
      GetTodosUseCaseParams params) async {
    final controller = StreamController<GetTodosUseCaseResponse>();
    try {
      // get user
      final todos = await todoRepository.getTodos();
      // Adding it triggers the .onNext() in the `Observer`
      // It is usually better to wrap the reponse inside a respose object.
      controller.add(GetTodosUseCaseResponse(todos));
      logger.finest('GetTodosUseCase successful.');
      controller.close();
    } catch (e) {
      logger.severe('GetTodosUseCase unsuccessful.');
      // Trigger .onError
      controller.addError(e);
    }
    return controller.stream;
  }
}

class GetTodosUseCaseParams {}

class GetTodosUseCaseResponse {
  final List<Todo> todos;
  GetTodosUseCaseResponse(this.todos);
}

