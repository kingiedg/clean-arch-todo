import '../../domain/entities/todo.dart';
import '../../domain/repositories/todo_repository.dart';


class DataTodosRepository extends TodoRepository {
  
  List<Todo> todos;
  static final DataTodosRepository _instance = DataTodosRepository._internal();
  DataTodosRepository._internal(){
    todos = [];
    todos.add(Todo(title: "Yeah", done: false));
    todos.add(Todo(title: "Wew", done: false));
  }
  
  factory DataTodosRepository() => _instance;

  @override
  Todo addTodo(Todo todo){
    print("ADDING TODO ...");
    // final Todo todo = Todo(title: "Yeaasdadadah", done: false);
    todos.add(todo);
    print(todos);
    return todo;
  }

  @override
  List<Todo> getTodos(){
    print("GETTING TODOS ...");
    this.todos.forEach((val) => print(val));
    return this.todos;
  }
}