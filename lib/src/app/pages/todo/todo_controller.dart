import 'package:example/src/domain/entities/user.dart';
import 'package:example/src/domain/repositories/todo_repository.dart';

import './todo_presenter.dart';
import '../../../domain/entities/todo.dart';
import 'package:flutter/material.dart';
import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';


class TodoController extends Controller {
  // final TodoRepository todoRepo;
  final TodoPresenter todoPresenter;

  List<Todo> todos;
  List<Todo> get todoList => todos;
  TodoController(todoRepo) : todoPresenter = TodoPresenter(todoRepo), super();

  @override
  void initListeners(){
    todoPresenter.addTodoOnNext = (Todo todo){
      print("ADDED TODO ${todo}");
      refreshUI();
    };
    todoPresenter.addTodoOnComplete = ()=>print("ON COMPLETE");
    todoPresenter.addTodoOnError = (e){
      print("ERROR OCCURED ${e.toString()}");
      ScaffoldState state = getState();
      state.showSnackBar(SnackBar(content: Text(e.message)));
      refreshUI();
    };

    todoPresenter.getTodosOnNext = (response){
      print("GET TODOSSSSSS $response");
      this.todos = response;
      refreshUI();
    };
    this.getTodos();
  }

  void addTodo(String title){
    todoPresenter.addTodo(new Todo(
      title: title,
      done: false
    ));

    this.getTodos();
  }
  
  void getTodos() => todoPresenter.getTodos();


}                                                                                                                                           