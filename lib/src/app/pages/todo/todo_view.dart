import './todo_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';
import '../../../data/repositories/data_todo_repository.dart';




class TodoPage extends View {
  TodoPage({Key key}) : super(key: key);
  
  @override
  _TodoPageState createState() => _TodoPageState();
}

class _TodoPageState extends ViewState<TodoPage, TodoController> {
  _TodoPageState() : super(TodoController(DataTodosRepository()));
  
  @override
  Widget get view {
    return Scaffold(
      appBar: AppBar(
        title: Text("TODO WEW"),
      ),
      body: Scaffold(
        key: globalKey,
        body: ControlledWidgetBuilder<TodoController>(
        builder: (context, controller) {
            print("sadadad ${controller.todoList}");
            return controller.todoList!=null ? ListView.builder(
              itemCount: controller.todoList.length,
              itemBuilder: (context, index) {
                return ListTile(
                  title: Text("${controller.todoList[index].title ?? ''}"),
                );
              },
            ) : Container();
          },
        )
      ),
      floatingActionButton: ControlledWidgetBuilder<TodoController>(
        builder: (context, controller) {
          return FloatingActionButton(
            onPressed: () async {
              // controller.addTodo();
              final add = await showDialog(
                context: context,
                builder: (BuildContext buildContext) {
                  String todo_title = "";
                return StatefulBuilder(
                  builder: (context, setStateInner) {
                    return SimpleDialog(
                      backgroundColor: Colors.white,
                      title: Text("Add Todo"),
                      contentPadding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 20.0),
                      children: [
                        TextFormField(
                          onChanged: (val){
                            print("VALVALVAL $val");
                            setStateInner((){
                              todo_title = val;
                            });
                          },
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(24),
                              borderSide: BorderSide(
                                width: 1,
                                color: const Color(0xffffffff)
                              )
                            ),
                            hintText: "Todo",
                            labelText: "Add Todo",
                            hintStyle: TextStyle(
                              color: Color(0xff787993),
                              fontWeight: FontWeight.w400,
                              fontFamily: "SFProDisplay",
                              fontStyle: FontStyle.normal,
                              fontSize: 14.0
                            ),
                            contentPadding: EdgeInsets.all(14.0)
                          )
                        ),
                        MaterialButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0)),
                          color: Colors.blue[600],
                          height: 40.0,
                          child: Text(
                            "Add",
                            style: TextStyle(color: Colors.white, fontSize: 13.0),
                          ),
                          onPressed: () async {
                            Navigator.pop(buildContext, todo_title);
                          },
                        ),
                      ],
                    );
                  });
                }
              );
              
              controller.addTodo(add);
            },
            tooltip: 'Add Todo',
            child: Icon(Icons.add),
          );
        },
      ),
    );
  }
}