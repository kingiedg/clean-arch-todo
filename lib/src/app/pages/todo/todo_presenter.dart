import '../../../domain/usecases/add_todo_usecase.dart';
import '../../../domain/entities/todo.dart';
import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';


class TodoPresenter extends Presenter {
  Function addTodoOnNext;
  Function addTodoOnComplete;
  Function addTodoOnError;
  
  Function getTodosOnNext;
  
  final AddTodoUseCase addTodoUseCase;
  final GetTodosUseCase getTodosUseCase;
  TodoPresenter(todoRepo) : addTodoUseCase = AddTodoUseCase(todoRepo), getTodosUseCase = GetTodosUseCase(todoRepo);

  void addTodo(Todo todo) {
    addTodoUseCase.execute(
        _AddTodoUseCaseObserver(this), AddTodoUseCaseParams(todo));
  }
  void getTodos() {
    getTodosUseCase.execute(
        _GetTodosUseCaseObserver(this), GetTodosUseCaseParams());
  }
  

  @override
  void dispose() {
    addTodoUseCase.dispose();
    getTodosUseCase.dispose();
  }
}

class _AddTodoUseCaseObserver extends Observer<AddTodoUseCaseResponse> {
  final TodoPresenter presenter;
  
  _AddTodoUseCaseObserver(this.presenter);
  @override
  void onComplete() {
    assert(presenter.addTodoOnComplete != null);
    presenter.addTodoOnComplete();
  }

  @override
  void onError(e) {
    assert(presenter.addTodoOnError != null);
    presenter.addTodoOnError(e);
  }

  @override
  void onNext(response) {
    print("PRESEENTER ${response}");
    assert(presenter.addTodoOnNext != null);
    presenter.addTodoOnNext(response.todo);
  }

}

class _GetTodosUseCaseObserver extends Observer<GetTodosUseCaseResponse> {
  final TodoPresenter presenter;
  
  _GetTodosUseCaseObserver(this.presenter);
  @override
  void onComplete() {
    print("ONCOMPLETE  GET TODOS");
  }

  @override
  void onError(e) {
    print("ON ERROR GET TODOS");
  }

  @override
  void onNext(GetTodosUseCaseResponse response) {
    print("ONNEXT  GET TODOS");
    presenter.getTodosOnNext(response.todos);
  }

}